<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\HelperClass;
use App\Taxi;
use Image;
use Auth;

class TaxiController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

    	$cars = Taxi::where('user_id',Auth::user()->id)->where('status',HelperClass::$APPROVE)->get();
    	return view('pages.taxi.index')->with('cars',$cars);
    }
    public function addnewcar(Request $request)
    {
      //return response()->json($request->all());
    	//dd($request->all());
    	$insert = new Taxi;
    	$insert->user_id = Auth::user()->id;
    	$insert->vahical_name = $request->car_name;
      $insert->referral = $request->referral;
    	$insert->security_number = $request->securitynidnumber;
    	$insert->rate_per_our = $request->rate;
    	$insert->location = $request->location;
    	$insert->condition_note = $request->car_condition;
    	if ($request->hasFile('license')) {
          $image = $request->file('license');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          $location = 'uploads/license/' . $filename;
          Image::make($image)->resize(300,250)->save($location);
          $insert->license_image = $filename;
        }
        if ($request->hasFile('regdocfile')) {
          $image = $request->file('regdocfile');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          $location = 'uploads/registration_images/' . $filename;
          Image::make($image)->resize(300,250)->save($location);
          $insert->registration_images = $filename;
        }
        if ($request->hasFile('car_image')) {
          $image = $request->file('car_image');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          $location = 'uploads/car_image/' . $filename;
          Image::make($image)->resize(300,250)->save($location);
          $insert->car_image = $filename;
        }
    	if($insert->save())
    	{
    		$success = "Car added successfully please wait for approval";
    		return response()->json($success);
    	}
    }
    public function updatecar(Request $request)
    {
      $update = Taxi::where('id',$request->id)->update([
        'vahical_name' => $request->car_name,
        'referral' => $request->referral,
        'security_number' => $request->securitynidnumber,
        'rate_per_our' => $request->rate,
        'location' => $request->location,
        'condition_note' => $request->car_condition,
        'status' => HelperClass::$PENDING,
      ]);
      if ($request->hasFile('license')) {
        $image = $request->file('license');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $location = 'uploads/license/' . $filename;
        Image::make($image)->resize(300,250)->save($location);
        $update = Taxi::where('id',$request->id)->update(['license_image' => $filename]);
      }
      if ($request->hasFile('regdocfile')) {
        $image = $request->file('regdocfile');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $location = 'uploads/registration_images/' . $filename;
        Image::make($image)->resize(300,250)->save($location);
        $update = Taxi::where('id',$request->id)->update(['registration_images' => $filename]);
      }
      if ($request->hasFile('car_image')) {
        $image = $request->file('car_image');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $location = 'uploads/car_image/' . $filename;
        Image::make($image)->resize(300,250)->save($location);
        $update = Taxi::where('id',$request->id)->update(['car_image' => $filename]);
      }
      return response()->json('success');
    }
    public function usertaxi()
    {
      $data = Taxi::where('user_id',Auth::user()->id)->get();
      return view('pages.taxi.viewtaxi')->with('cars',$data);
    }
    public function getactivityjson($id = null)
    {
      if($id != null)
      {
        $data = Taxi::where('user_id',$id)->get();
        return response()->json($data); 
      }
      else
      {
        $data = Taxi::where('user_id',Auth::user()->id)->get();
        return response()->json($data);
      }
      return response()->json('success');
    }
    public function gettaxidetails($id)
    {
      $taxi = Taxi::where('id',$id)->first();
      return response()->json($taxi); 
    }
    public function delete_car($id)
    {
      $delete = Taxi::find($id);
      $delete->delete();
      $getuser = Taxi::where('id',$id)->first();
      if(count(Auth::user()->isUserHasTaxiPermission()) >= 1 && isset($getuser->user_id))
      {
        $data = Taxi::where('user_id',$getuser->user_id)->get();
        return response()->json($data); 
      }
      else
      {
        $data = Taxi::where('user_id',Auth::user()->id)->get();
        return response()->json($data); 
      }
    }
    public function approve_car($id)
    {
      $approve = Taxi::where('id',$id)->update(['status'=>HelperClass::$APPROVE]);
      $getuser = Taxi::where('id',$id)->first();
      if(isset($getuser->user_id))
      {
        $data = Taxi::where('user_id',$getuser->user_id)->get();
        return response()->json($data); 
      }
      else
      {
        $data = Taxi::where('user_id',Auth::user()->id)->get();
        return response()->json($data); 
      }
    }
    public function reject_car($id)
    {
      $reject = Taxi::where('id',$id)->update(['status'=>HelperClass::$REJECTED]);
      $getuser = Taxi::where('id',$id)->first();
      if(isset($getuser->user_id))
      {
        $data = Taxi::where('user_id',$getuser->user_id)->get();
        return response()->json($data); 
      }
      else
      {
        $data = Taxi::where('user_id',Auth::user()->id)->get();
        return response()->json($data); 
      }
    }
}
