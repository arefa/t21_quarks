<?php

use App\Balance as Balance;
use App\SavedPost as SavedPost;

function isFollowing($userId, $followableId)
{
    $followStatus = false;
    $isFollowing = (new \App\Follow())->where('user_id', $userId)->where('followable_id', $followableId)->first();
    if ($isFollowing) {
        if ($isFollowing->status == 1) {
            $followStatus = true;
        }
    }

    return $followStatus;

}

function userReview($userId, $reviewable_id, $bidId = null)
{
    $reviewNumber = 0;
    if ($bidId) {
        $isReviewExist = (new \App\Review())->where('user_id', $userId)->where('reviewable_id', $reviewable_id)->where('bid_id', $bidId)->first();
    } else {
        $isReviewExist = (new \App\Review())->where('user_id', $userId)->where('reviewable_id', $reviewable_id)->first();
    }
    if ($isReviewExist) {
        $reviewNumber = $isReviewExist->review_number;
    }

    return $reviewNumber;
}

function averageReview($userId)
{

    $averageReview = (new \App\Review())->where('reviewable_id', $userId)->avg('review_number');

    return number_format($averageReview, 1);
}

function totalReview($userId)
{


    $totalReview = (new \App\Review())->where('reviewable_id', $userId)->count();

    return $totalReview;
}

function isEventFavorited($eventId)
{
    $event = \App\Event::find($eventId);


    return $event->isFavorited();
}

function getPostBidOrders($buyer, $type = 'buy')
{
    $orders = \App\PostBid::where(['reference_id' => $buyer->id, 'bid_type' => $type])->where('status', '!=', 'pending')->count();
    return $orders;
}

function getPostTotalBids($buyer, $type = 'buy')
{
    $orders = \App\PostBid::where(['reference_id' => $buyer->id, 'bid_type' => $type])->count();
    return $orders;
}

function bidAmountIfAlreadyBid($buyer, $type = 'buy')
{
    $amount = '';
    if (auth()->check()) {
        $bid = $buyer->bids()->where('user_id', auth()->id())->first();
        if ($bid) $amount = $bid->amount;
    }
    return $amount;
}

function isSavedPost($post_id, $type, $user_id)
{
    $checkExist = SavedPost::where('user_id', '=', $user_id)->where('post_id', '=', $post_id)->where('post_type', '=', $type)->first();
    if ($checkExist)
        return true;
    else
        return false;
}

function getCurrentRate($buyer, $type = 'buy')
{
    $bids = $buyer->bids;
    if ($bids->isNotEmpty()) {
        return $bids->min('amount');
    }
    return $buyer->rate;

}

function calculateTotalAvailableBalance($id)
    {
        $credit_sum = 0;
        $debit_sum  = 0;
        $transactions    =  Balance::where('user_id','=',$id)->orderBy('id', 'DESC')->get()->toArray();

        foreach($transactions as $key=>$value)
        {
            if($value['type'] == 'cr'){
                $credit_sum += $value['amount'];
            }
            else if($value['type'] == 'db'){
                if($value['withdraw']==""){
                    $debit_sum  += $value['amount'];
                }
                else{
                    $debit_sum  += $value['withdraw'];
                }
            }

        }
        $total_balance_remained  =  $credit_sum - $debit_sum;
        return $total_balance_remained;
    }

?>


