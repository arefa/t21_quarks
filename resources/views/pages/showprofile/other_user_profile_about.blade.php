@extends('layouts.app')
@section('custom-styles')

@endsection

@section('content')
    @include('partials.other_user_profile')
    <br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-sm-12 col-xs-12">

                <!-- <a href="{{url('generate-pdf')}}"><img src="{{url('img/download.png')}}" style="height: 25px ;width: 33px;"> </a>

                <a href="#" style="padding-top:0.25rem; margin-left: 2.75rem; color:#00CD00;" data-toggle="modal"
                   data-target="#myModal"><i class="fas fa-play-circle" style="padding-right: 1rem;"></i>Play Video</a>
                <i class="fa fa-map-marker" aria-hidden="true" style="margin-left: 0"> United State | </i> -->

                
            </div>
        </div>
    </div>


    <div class="modal fade" id="playVideo" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Video Link</h4>
                </div>
                <div class="modal-body">
                    <input type="text" onkeyup="videoFunction()" id="videoId" class="form-control mr-sm-2"
                           placeholder="Youtube Link">
                    <div class="col-md-6">
                        <iframe src="https://www.youtube.com/watch?v=UqNJb0mWJ20" id="iframe_link" width="400"
                                height="300" frameborder="0" allowfullscreen></iframe>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('extra-JS')

@endsection