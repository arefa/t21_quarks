@extends('layouts.app')
@section('content')
<!--- image style -->
    <style>
        .takephoto{
  background-color: #fff;
  border-color: #000;
  border-width: 1px solid;
  padding: 10px;
  width: 100%;
  text-align: center;
  border-radius: 5px;
}
.continue{
  background-color: #006dbc;
  border-color: #006dbc;
  box-shadow: gray;
  padding: 10px;
  width: 100%;
  text-align: center;
  border-radius: 5px;
  color: #fff;
}
.button-blue{
  box-shadow: gray;
  padding: 20px;
  width: 100%;
  text-align: left;
  border-radius: 3px;
  color: #fff;
}
.button-blue:hover{
  background-color: #006dbc;
  border-color: #006dbc;
}
.button-blue:focus{
  background-color: #006dbc;
  border-color: #006dbc;
}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right!import;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
        #myImgm {
            border-radius: 5px;
            cursor: pointer;
            transition: 0.3s;
        }

        #myImgm:hover {
            opacity: 0.7;
        }

        /* The Modal (background) */
        .modalmatul {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
        }

        /* Modalmatul Content (image) */
        .modalmatul-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
        }

        /* Caption of Modalmatul Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
        }

        /* Add Animation */
        .modalmatul-content, #caption {
            -webkit-animation-name: zoom;
            -webkit-animation-duration: 0.6s;
            animation-name: zoom;
            animation-duration: 0.6s;
        }

        @-webkit-keyframes zoom {
            from {
                -webkit-transform: scale(0)
            }
            to {
                -webkit-transform: scale(1)
            }
        }

        @keyframes zoom {
            from {
                transform: scale(0)
            }
            to {
                transform: scale(1)
            }
        }

        /* The Close Button */
        #close {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        #close:hover,
        #close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

        #close2 {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        #close2:hover,
        #close2:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px) {
            .modalmatul-content {
                width: 100%;
            }
        }
    </style>
@include('partials._user-profile')
<div class="container">
    <div class="row">
      @foreach($cars as $obj)
      <div class="col-md-3" style="padding: 5px;">
        <div class="card mb-3" style="width: 17rem;">
          <img class="card-img-top" src="{{asset('uploads/car_image')}}/{{$obj->car_image}}" alt="Card image cap">
          <div  style="width: 100%;padding: 10px;">
            <div style="display: inline-block;">
              <strong>{{$obj->location}}</strong>
            </div><br>
            <div style="display: inline-block;">
              <strong>${{$obj->rate_per_our}}/hr</strong>
            </div>
            <div style="display: inline-block;">
              <span style="float: right;margin-left: 150px;">
                <a style="background-color: #006dbc ;padding: 10px;color: #000" href="#" >Book now</a>
              </span>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
</div>
<form method="POST" action="addnewcar" id="addnewcarform" enctype="multipart/form-data">
@csrf
<input type="hidden" id="taxiId" name="id">
<div id="accountstatus" class="modal">
  <div class="modal-content col-sm-9 col-md-5">
    <div style="flex-flow: column;">
      <a onclick="closemod();"><i class="fas fa-arrow-left" style="align-self: flex-start;"></i></a>
    </div>
    <div>
      <div style="padding-top: 25px;padding-bottom: 10px;">
        <div>
          <center>
            <img src="{{asset('images/taxit.png')}}" class="img-thumbnail"><br>
            <p>Account status</p>
            <h5>In review</h5>
            <p>Review may take a few days</p>
          </center>
        </div><br>
        <p>Your Progress</p>
        <!-- <table id="statustab" class="table" style="border: none;">
        </table> -->
        <div id="statustab">
            
        </div>
    </div>  
  </div>
</div>
</div>
<div id="backgroundcheck" class="modal">
  <div class="modal-content col-sm-9 col-md-4">
    <div style="flex-flow: column;">
      <a onclick="fun2();"><i class="fas fa-arrow-left" style="align-self: flex-start;"></i></a>
    </div>
  <div>
    <div style="padding-top: 25px;padding-bott">
      <h4 style="display: block;">For  authentication purposes, we need your Social Security Number for a background check</h4>
        <label>Social security /NID number </label>
        <div class="input-group">
          <input type="text" class="form-control" id="securitynidnumber" name="securitynidnumber">
          <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-lock"></i></span>
          </div>
        </div>
        <div style="padding-top: 10px;padding-bottom: 10px;">
          <p>We Partners must go through a background check to maintain safe expirience on the road</p>
        </div>
        <div>
          <p><i class="fas fa-check"></i> Personal information is protected with bank-lavel-security</p>
          <p><i class="fas fa-check"></i> no Credit check -credit won't be affected</p>
        </div>
    </div>
    <span>
      <button class="continue" onclick="changephotoofdrivinglicense()" type="button">COUNTINUE</button>
    </span>
  </div>  
</div>
</div>
<div id="uploadlicence" class="modal">
  <div class="modal-content col-sm-9 col-md-4">
    <div style="flex-flow: column;">
      <a onclick="fun4();"><i class="fas fa-arrow-left" style="align-self: flex-start;"></i></a>
    </div>
    <div>
      <div style="padding-top: 25px;padding-bottom: 10px;">
        <h4 style="display: block;">Take a photo of your Driver's License</h4>
          <div style="padding-top: 10px;padding-bottom: 10px;">
            <p>Make sure your Driver's License is not expired and avoid using the flase so that your information is clear and visible</p>
            <div>
              <img src="images/Taxi.png" id="output" class="img-thumbnail">
            </div>
          </div>
      </div>
      <span>
        <input type="file" id="licensefile" onchange="loadFile(event)" hidden name="license">
        <button class="takephoto" onclick="takephoto()" type="button">Take photo with my phone</button>
      </span><br><br>
      <span>
        <button class="continue" onclick="licenseimgupload()" type="button">UPLOAD PHOTO</button>
      </span>
    </div>  
  </div>
</div>
<div id="licenceview" class="modal">
  <div class="modal-content col-sm-9 col-md-4">
    <div style="flex-flow: column;">
      <a onclick="fun4();"><i class="fas fa-arrow-left" style="align-self: flex-start;"></i></a>
    </div>
    <div>
      <div style="padding-top: 25px;padding-bottom: 10px;">
        <h4 style="display: block;">Take a photo of your Driver's License</h4>
          <div style="padding-top: 10px;padding-bottom: 10px;">
            <p>Make sure your Driver's License is not expired and avoid using the flase so that your information is clear and visible</p>
            <div>
              <img src="images/Taxi.png" id="licenceviewimage" class="img-thumbnail">
            </div>
          </div>
      </div><br><br>
      <span>
        <button class="continue" onclick="regdocimgviewfun()" type="button">Next</button>
      </span>
    </div>  
  </div>
</div>
<div id="regdocimgview" class="modal">
  <div class="modal-content col-sm-9 col-md-4">
    <div style="flex-flow: column;">
      <a onclick="fun5();"><i class="fas fa-arrow-left" style="align-self: flex-start;"></i></a>
    </div>
    <div>
      <div style="padding-top: 25px;padding-bottom: 10px;">
        <h4 style="display: block;">Photo of your CA Vehicle Registration</h4>
          <div style="padding-top: 10px;padding-bottom: 10px;">
            <p>Make sure your vehicle's make,model,year,license plate,VIN and expiration are clear and visible</p>
            <div>
              <img src="images/Taxica.png" id="viedoc" class="img-thumbnail">
            </div>
          </div>
      </div><br><br>
      <span>
        <button class="continue" onclick="nextviewdetails()" type="button">Next</button>
      </span>
    </div>  
  </div>
</div>
<div id="csvehicleregi" class="modal">
  <div class="modal-content col-sm-9 col-md-4">
    <div style="flex-flow: column;">
      <a onclick="fun5();"><i class="fas fa-arrow-left" style="align-self: flex-start;"></i></a>
    </div>
    <div>
      <div style="padding-top: 25px;padding-bottom: 10px;">
        <h4 style="display: block;"> photo of your CA Vehicle Registration</h4>
          <div style="padding-top: 10px;padding-bottom: 10px;">
            <p>Make sure your vehicle's make,model,year,license plate,VIN and expiration are clear and visible</p>
            <div>
              <img src="images/Taxica.png" id="output2" class="img-thumbnail">
            </div>
          </div>
      </div>
      <span>
        <input type="file" id="regdocfile" onchange="document.getElementById('output2').src = window.URL.createObjectURL(this.files[0])" hidden name="regdocfile">
        <button class="takephoto" onclick="takephotoca()" type="button">Take photo with my phone</button>
      </span><br><br>
      <span>
        <button class="continue" onclick="regdocimgupload()" type="button">UPLOAD PHOTO</button>
      </span>
    </div>  
  </div>
</div>
<div id="details" class="modal">
  <div class="modal-content col-sm-9 col-md-4">
    <div style="flex-flow: column;">
      <a onclick="fun6();"><i class="fas fa-arrow-left" style="align-self: flex-start;"></i></a>
    </div>
    <div>
      <div style="padding-top: 25px;padding-bottom: 10px;">
        <div class="row">
          <div class="col">
            <label>Rate per hour</label>
            <input type="text" name="rate" id="rate" class="form-control">
          </div>
          <div class="col">
            <label>Referral</label>
            <input type="text" name="referral" id="referral" class="form-control">
          </div>
        </div>
        <div class="form-group">
          <label>Location</label>
          <input type="text" name="location" id="location" class="form-control">
        </div>
        <div class="form-group">
          <label>Car model name</label>
          <input type="text" name="car_name" id="carmodelname" class="form-control">
        </div>
        <div class="form-group">
          <label>Car condition</label>
          <textarea class="form-control" name="car_condition" id="carcondition"></textarea>
        </div>
        <div>
          <label>Car Image</label>
          <div>
            <img src="images/Taxica.png" id="output3" class="img-thumbnail">
          </div>
          <input type="file" id="car_image" onchange="document.getElementById('output3').src = window.URL.createObjectURL(this.files[0])" hidden name="car_image">
          <button class="takephoto" onclick="takephotocar()" type="button">Take photo with my phone</button>
        </div>
      </div>
      <span>
        <button class="continue" onclick="confirm()" type="button">CONTINUE</button>
      </span>
    </div>  
  </div>
</div>
<div id="viewdetails" class="modal">
  <div class="modal-content col-sm-9 col-md-4">
    <div style="flex-flow: column;">
      <a onclick="fun6();"><i class="fas fa-arrow-left" style="align-self: flex-start;"></i></a>
    </div>
    <div>
      <div style="padding-top: 25px;padding-bottom: 10px;">
        <div class="form-group">
          <label>Rate per hour</label>
          <input type="text" name="rate" disabled id="rateview" class="form-control">
        </div>
        <div class="form-group">
          <label>Location</label>
          <input type="text" name="location" disabled id="locationview" class="form-control">
        </div>
        <div class="form-group">
          <label>Car model name</label>
          <input type="text" name="car_name" disabled id="carmodelnameview" class="form-control">
        </div>
        <div class="form-group">
          <label>Car condition</label>
          <textarea class="form-control" name="car_condition" disabled id="carconditionview"></textarea>
        </div>
        <div>
          <label>Car Image</label>
          <div>
            <img src="images/Taxica.png" id="carimage" class="img-thumbnail">
          </div>
        </div>
      </div>
      <span>
        <button class="continue" onclick="closedetails()" type="button">Back</button>
      </span>
    </div>  
  </div>
</div>
<div id="congrats" class="modal">
  <div class="modal-content col-sm-9 col-md-4">
    <div>
      <div style="padding-top: 25px;padding-bottom: 10px;">
        <h5>Congrats! You're all done</h5>
        <br>
        <p>You'll be notified by text and email once you're approved to drive</p>
        <div>
          <center>
            <img src="{{asset('images/Taxic.png')}}" class="img-thumbnail">
          </center>
        </div>
        <br>
        <p>you anderstand that, in order to access the Uber app, you will we obligated to transoprt with service animals in accordance with aplicable fedral ,state and local lawa and <a> Uber's Service Animal Policy </a></p>
      <span>
        <button class="continue" onclick="accountstatusfun()" type="button">CHECK MY STATUS</button>
      </span>
    </div>  
  </div>
</div>
</div>
</form>
<div id="backgroundcheckview" class="modal">
  <div class="modal-content col-sm-9 col-md-4">
    <div style="flex-flow: column;">
      <a onclick="fun2();"><i class="fas fa-arrow-left" style="align-self: flex-start;"></i></a>
    </div>
  <div>
    <div style="padding-top: 25px;padding-bott">
      <h4 style="display: block;">For  authentication purposes, we need your Social Security Number for a background check</h4>
        <label>Social security /NID number </label>
        <div class="input-group">
          <input type="text" class="form-control" id="viewsecuritynidnumber" disabled name="securitynidnumber">
          <div class="input-group-append">
            <span class="input-group-text"><i class="fas fa-lock"></i></span>
          </div>
        </div>
        <div style="padding-top: 10px;padding-bottom: 10px;">
          <p>We Partners must go through a background check to maintain safe expirience on the road</p>
        </div>
        <div>
          <p><i class="fas fa-check"></i> Personal information is protected with bank-lavel-security</p>
          <p><i class="fas fa-check"></i> no Credit check -credit won't be affected</p>
        </div>
    </div>
    <span>
      <button class="continue" onclick="nextlicenseimage()" type="button">Next</button>
    </span>
  </div>  
</div>
</div>
<script type="text/javascript">
var accountstatus = document.getElementById('accountstatus');
var backgroundcheck = document.getElementById('backgroundcheck');
var backgroundcheckview = document.getElementById('backgroundcheckview');
var uploadlicence = document.getElementById('uploadlicence');
var licenceview = document.getElementById('licenceview');
var csvehicleregi = document.getElementById('csvehicleregi');
var regdocimgview = document.getElementById('regdocimgview');
var details = document.getElementById('details');
var viewdetails = document.getElementById('viewdetails');
var congrats = document.getElementById('congrats');

function takephoto(){
  document.getElementById('licensefile').click();
}
function takephotoca(){
  document.getElementById('regdocfile').click(); 
}
function takephotocar(){
  document.getElementById('car_image').click();  
}
function viewcar(id)
{
    $.ajax({
        url: "gettaxidetails/"+id, 
        type: "get",    
        dataType:"json",   
        success: function (response) 
        {
            console.log(response.license_image);
            var security = response.security_number;
            var output = document.getElementById('licenceviewimage');
            var output2 = document.getElementById('viedoc');
            var output3 = document.getElementById('carimage');
            $('#viewsecuritynidnumber').val(security);
            $('#rateview').val(response.rate_per_our);
            $('#locationview').val(response.location);
            $('#carmodelnameview').val(response.vahical_name);
            $('#carconditionview').val(response.condition_note);
            $('#taxiId').val(response.id);
            output.src = "uploads/license/"+response.license_image;
            output2.src = "uploads/registration_images/"+response.registration_images;
            output3.src = "uploads/car_image/"+response.car_image;
        }   
    });
    accountstatus.style.display = "none";
    backgroundcheckview.style.display = "block";
}
function nextlicenseimage()
{
    backgroundcheckview.style.display = "none";
    licenceview.style.display = "block";
}
function regdocimgviewfun(){
    licenceview.style.display = "none";   
    regdocimgview.style.display = "block";
}
function nextviewdetails()
{
    regdocimgview.style.display = "none";
    viewdetails.style.display = "block";   
}
function changephotoofdrivinglicense(){
    backgroundcheck.style.display = "none";
    uploadlicence.style.display = "block";
}
function regdocimgupload(){
  csvehicleregi.style.display = "none";
  details.style.display = "block"; 
}
function licenseimgupload(){
  uploadlicence.style.display = "none";
  csvehicleregi.style.display = "block";
}
function accountstatusfun(){
    congrats.style.display = "none";
    $.ajax({
        url: "getactivityjson", 
        type: "get",    
        dataType:"json",   
        success: function (response) 
        {
          var table = $('#statustab');
          table.empty();
          response.forEach(function (value) {
            if (value.status == 1) {sview = "Approved"}else{ sview = "In review"}
              table.append("<div class='alert' role='alert' style='display: inline-flex'>"+
                            "<p style='display:inline-flex' onclick='viewcar("+value.id+")'><i class='fa fa-clock' style='font-size:36px;margin-right: 10px;'></i>"+value.vahical_name +"<br>"+ sview +"</p>"+
                            "<span class='float-right' style='margin-top: 10px;margin-left: 100px;'>"+
                                "<a href='#' onclick='edit("+value.id+")' style='margin :3px;padding: 10px;background-color: #9a9a9a;color: black;'>Edit</a>"+
                                "<a href='#' onclick='delete_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #f04726;color: black;'>Delete</a>"+@if(count(Auth::user()->isUserHasTaxiPermission()) >= 1)
                                "<a href='#' onclick='approve_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #5eb44c;color: black;'>Approve</a>"+
                                "<a href='#' onclick='reject_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #f47e2b;color: black;'>Cancel</a>"+@endif
                            "</span>"
                            +"</div>");
          });
        }   
    });
    accountstatus.style.display = "block";
}
function delete_car(id)
{
    $.ajax({
        url: "delete_car/"+id, 
        type: "get",    
        dataType:"json",   
        success: function (response) 
        {
          var table = $('#statustab');
          table.empty();
          response.forEach(function (value) {
            if (value.status == 1) {sview = "Approved"}else{ sview = "In review"}
               table.append("<div class='alert' role='alert' style='display: inline-flex'>"+
                            "<p style='display:inline-flex' onclick='viewcar("+value.id+")'><i class='fa fa-clock' style='font-size:36px;margin-right: 10px;'></i>"+value.vahical_name +"<br>"+ sview +"</p>"+
                            "<span class='float-right' style='margin-top: 10px;margin-left: 100px;'>"+
                                "<a href='#' onclick='edit("+value.id+")' style='margin :3px;padding: 10px;background-color: #9a9a9a;color: black;'>Edit</a>"+
                                "<a href='#' onclick='delete_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #f04726;color: black;'>Delete</a>"+@if(count(Auth::user()->isUserHasTaxiPermission()) >= 1)
                                "<a href='#' onclick='approve_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #5eb44c;color: black;'>Approve</a>"+
                                "<a href='#' onclick='reject_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #f47e2b;color: black;'>Cancel</a>"+@endif
                            "</span>"
                            +"</div>");
          });
        }   
    });
    accountstatus.style.display = "block";
}
function approve_car(id)
{
    $.ajax({
        url: "approve_car/"+id, 
        type: "get",    
        dataType:"json",   
        success: function (response) 
        {
          var table = $('#statustab');
          table.empty();
          response.forEach(function (value) {
            if (value.status == 1) {sview = "Approved"}else{ sview = "In review"}
               table.append("<div class='alert' role='alert' style='display: inline-flex'>"+
                            "<p style='display:inline-flex' onclick='viewcar("+value.id+")'><i class='fa fa-clock' style='font-size:36px;margin-right: 10px;'></i>"+value.vahical_name +"<br>"+ sview +"</p>"+
                            "<span class='float-right' style='margin-top: 10px;margin-left: 100px;'>"+
                                "<a href='#' onclick='edit("+value.id+")' style='margin :3px;padding: 10px;background-color: #9a9a9a;color: black;'>Edit</a>"+
                                "<a href='#' onclick='delete_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #f04726;color: black;'>Delete</a>"+@if(count(Auth::user()->isUserHasTaxiPermission()) >= 1)
                                "<a href='#' onclick='approve_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #5eb44c;color: black;'>Approve</a>"+
                                "<a href='#' onclick='reject_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #f47e2b;color: black;'>Cancel</a>"+@endif
                            "</span>"
                            +"</div>");
          });
        }   
    });
    accountstatus.style.display = "block";
}
function reject_car(id) {
    $.ajax({
        url: "reject_car/"+id, 
        type: "get",    
        dataType:"json",   
        success: function (response) 
        {
          var table = $('#statustab');
          table.empty();
          response.forEach(function (value) {
            if (value.status == 1) {sview = "Approved"}else{ sview = "In review"}
               table.append("<div class='alert' role='alert' style='display: inline-flex'>"+
                            "<p style='display:inline-flex' onclick='viewcar("+value.id+")'><i class='fa fa-clock' style='font-size:36px;margin-right: 10px;'></i>"+value.vahical_name +"<br>"+ sview +"</p>"+
                            "<span class='float-right' style='margin-top: 10px;margin-left: 100px;'>"+
                                "<a href='#' onclick='edit("+value.id+")' style='margin :3px;padding: 10px;background-color: #9a9a9a;color: black;'>Edit</a>"+
                                "<a href='#' onclick='delete_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #f04726;color: black;'>Delete</a>"+@if(count(Auth::user()->isUserHasTaxiPermission()) >= 1)
                                "<a href='#' onclick='approve_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #5eb44c;color: black;'>Approve</a>"+
                                "<a href='#' onclick='reject_car("+value.id+")' style='margin :3px;padding: 10px;background-color: #f47e2b;color: black;'>Cancel</a>"+@endif
                            "</span>"
                            +"</div>");
          });
        }   
    });
    accountstatus.style.display = "block";
}
function closedetails(){
    viewdetails.style.display = "none";
}
function closemod(){
    accountstatus.style.display = "none";
}
function edit(id){
    $.ajax({
        url: "gettaxidetails/"+id, 
        type: "get",    
        dataType:"json",   
        success: function (response) 
        {
            console.log(response.license_image);
            var security = response.security_number;
            var output = document.getElementById('output');
            var output2 = document.getElementById('output2');
            var output3 = document.getElementById('output3');
            $('#securitynidnumber').val(security);
            $('#rate').val(response.rate_per_our);
            $('#referral').val(response.referral);
            $('#location').val(response.location);
            $('#carmodelname').val(response.vahical_name);
            $('#carcondition').val(response.condition_note);
            $('#taxiId').val(response.id);
            output.src = "uploads/license/"+response.license_image;
            output2.src = "uploads/registration_images/"+response.registration_images;
            output3.src = "uploads/car_image/"+response.car_image;
        }   
    });
    accountstatus.style.display = "none";
    backgroundcheck.style.display = "block";
}
function confirm(){
  var rate = document.getElementById("rate").value;
  var location = document.getElementById("location").value;
  var carmodelname = document.getElementById("carmodelname").value;
  var car_condition =  document.getElementById("carcondition").value;
  if(!rate)
  {
    alert("Please set Rate per hour");
    return;
  }
  if (!location) 
  {v
    alert("Please insert location");
    return;
  }
  if(!carmodelname)
  {
    alert("Please insert car model name");
    return;
  }
  if(!car_condition)
  {
    alert("Please insert car condition");
    return;
  }
  else
  {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  
    var form = $('#addnewcarform')[0];
    var data = new FormData(form);
    
    $.ajax({
      type:'POST',
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      cache: false,
      url:'/updatecar',
      data:data,
      success:function(data){
        console.log(data)
        var details = document.getElementById('details');
        var congrats = document.getElementById('congrats');
        details.style.display = "none"; 
        congrats.style.display = "block"; 
      }
    });
  }
}
</script>
<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
  var loadFileCA = function(event) {
    var output = document.getElementById('output2');
    output.src = URL.createObjectURL(document.getElementById("regdocfile").src);
  };
</script>
@endsection
