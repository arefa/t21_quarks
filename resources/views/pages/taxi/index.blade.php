@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3">
      <div class="card mb-3" style="width: 17rem;">
        <img class="card-img-top" src="/images/car.jpeg" style="padding: 10px;" alt="Card image cap">
        <span class="overlay_badge_sell">Sell</span>
        <strong style="align-self: center;">Name of car</strong>
        <div class="divback">
          <label style="padding-right: 33px;">Currentrate : <big class="bigtext"> US $2.01</big></label><label class="ordertext float-right">[ 5 orders]</label><br>
          <div style="flex-flow: column;">
            <label style="align-self: flex-start;">Auto order</label>
            <input type="text" name=""  size="4" style="align-self: center;">
            <button style="background-color: #0055a2;border-color: #0055a2;color: #fff">Bid</button>
            <button style="background-color: #f47e2b;border-color: #f47e2b;color: #fff;margin-right: 30px;"><i class="fas fa-close"></i></button><label class="bidtext">[ 7 bid ]</label>
            <h6 style="text-align: center; background-color: #f8f8f8">39m 32s Today 8:05Pm left</h6>
            <div style="padding: 5px;border-radius: 2px;">
              <i class="fas fa-share" style="font-size: 25px;color: #00a3e9"></i>&nbsp;345&nbsp;&nbsp;<span class="float-right" style="font-size: 20px;">2.7% Refferal</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card mb-3" style="width: 17rem;">
        <img class="card-img-top" src="/images/car.jpeg" style="padding: 10px;" alt="Card image cap">
        <span class="overlay_badge_buy">Buy</span>
        <strong style="align-self: center;">Name of car</strong>
        <div class="divback">
          <label style="padding-right: 33px;">Currentrate : <big class="bigtext"> US $2.01</big></label><label class="ordertext float-right">[ 5 orders]</label><br>
          <div style="flex-flow: column;">
            <label style="align-self: flex-start;">Auto order</label>
            <input type="text" name=""  size="4" style="align-self: center;">
            <button style="background-color: #0055a2;border-color: #0055a2;color: #fff">Bid</button>
            <button style="background-color: #f47e2b;border-color: #f47e2b;color: #fff;margin-right: 30px;"><i class="fas fa-close"></i></button><label class="bidtext float-right">[ 7 bid ]</label>
            <h6 style="text-align: center; background-color: #f8f8f8">39m 32s Today 8:05Pm left</h6>
            <div style="padding: 5px;border-radius: 2px;">
              <i class="fas fa-share" style="font-size: 25px;color: #00a3e9"></i>&nbsp;345&nbsp;&nbsp;<span class="float-right" style="font-size: 20px;">2.7% Refferal</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card mb-3" style="width: 17rem;">
        <img class="card-img-top" src="/images/car.jpeg" style="padding: 10px;" alt="Card image cap">
        <span class="overlay_badge_sell">Sell</span>
        <strong style="align-self: center;">Name of car</strong>
        <div class="divback">
          <label style="padding-right: 33px;">Currentrate : <big class="bigtext"> US $2.01</big></label><label class="ordertext float-right">[ 5 orders]</label><br>
          <div style="flex-flow: column;">
            <label style="align-self: flex-start;">Auto order</label>
            <input type="text" name=""  size="4" style="align-self: center;">
            <button style="background-color: #0055a2;border-color: #0055a2;color: #fff">Bid</button>
            <button style="background-color: #f47e2b;border-color: #f47e2b;color: #fff;margin-right: 30px;"><i class="fas fa-close"></i></button><label class="bidtext float-right">[ 7 bid ]</label>
            <h6 style="text-align: center; background-color: #f8f8f8">39m 32s Today 8:05Pm left</h6>
            <div style="padding: 5px;border-radius: 2px;">
              <i class="fas fa-share" style="font-size: 25px;color: #00a3e9"></i>&nbsp;345&nbsp;&nbsp;<span class="float-right" style="font-size: 20px;">2.7% Refferal</span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="card mb-3" style="width: 17rem;">
        <img class="card-img-top" src="/images/car.jpeg" style="padding: 10px;" alt="Card image cap">
        <span class="overlay_badge_buy">Buy</span>
        <strong style="align-self: center;">Name of car</strong>
        <div class="divback">
          <label style="padding-right: 33px;">Currentrate : <big class="bigtext"> US $2.01</big></label><label class="ordertext float-right">[ 5 orders]</label><br>
          <div style="flex-flow: column;">
            <label style="align-self: flex-start;">Auto order</label>
            <input type="text" name=""  size="4" style="align-self: center;">
            <button style="background-color: #0055a2;border-color: #0055a2;color: #fff">Bid</button>
            <button style="background-color: #f47e2b;border-color: #f47e2b;color: #fff;margin-right: 30px;"><i class="fas fa-close"></i></button><label class="bidtext float-right">[ 7 bid ]</label>
            <h6 style="text-align: center; background-color: #f8f8f8">39m 32s Today 8:05Pm left</h6>
            <div style="padding: 5px;border-radius: 2px;">
              <i class="fas fa-share" style="font-size: 25px;color: #00a3e9"></i>&nbsp;345&nbsp;&nbsp;<span class="float-right" style="font-size: 20px;">2.7% Refferal</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container"> 
  <div class="row">
    @foreach($cars as $obj)
    <div class="col-md-3">
      <div class="card mb-3" style="width: 17rem;">
        <img class="card-img-top" src="{{asset('uploads/car_image')}}/{{$obj->car_image}}" alt="Card image cap">
        <div  style="width: 100%;padding: 10px;">
          <div style="display: inline-block;">
            <strong>{{$obj->location}}</strong>
          </div><br>
          <div style="display: inline-block;">
            <strong>${{$obj->rate_per_our}}/hr</strong>
          </div>
          <div style="display: inline-block;">
            <span style="float: right;margin-left: 150px;">
              <a style="background-color: #006dbc ;padding: 10px;color: #000" href="#" >Book now</a>
            </span>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
<style type="text/css">
.overlay_badge_sell {
  position: absolute;
  top: 10px;
  left: 10px;
  background-color: skyblue;
  color: white;
  padding-left: 10px;
  padding-right: 10px;
}
.overlay_badge_buy {
  position: absolute;
  top: 10px;
  left: 10px;
  background-color: green;
  color: white;
  padding-left: 10px;
  padding-right: 10px;
}
.divback{
  background-color: #e3e3e3;
  padding: 5px;
  margin: 5px;
}
.bigtext{
  color: black;
  font-weight: bolder;
}
.ordertext{
  color: #a658a6;
}
.bidtext{
  color: #4e66c4;
  align-self: flex-end;
}
</style>
@endsection