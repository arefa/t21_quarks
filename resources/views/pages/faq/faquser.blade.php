@extends('layouts.app')


@section('custom-styles')

    <style>

        .result {
            max-width: 800px;
            margin-top: 60px;
            margin-left: auto;
            margin-bottom: 60px;
            margin-right: auto;
        }

        .accordion {
            box-shadow: 0 1px 6px rgba(0, 0, 0, 0.15);
        }

        .accordion-item {
            position: relative;
        }

        .accordion-toggle {
            display: block;
            width: 100%;
            cursor: pointer;
            padding: 20px;
            margin: 0;
            border: 0;
            border-bottom: 1px solid #cdcdcd;
            background-color: #ffffff;
            font-size: 16px;
            font-weight: 600;
            line-height: inherit;
            text-align: left;
        }

        .accordion-toggle:hover {
            background-color: #f5f5f5;
        }

        .is-open > .accordion-toggle {
            background-color: #3b5998;
            color: #ffffff;
        }

        .accordion-panel {
            background-color: #ffffff;
            border-bottom: 1px solid #cdcdcd;
        }

        .accordion-panel > *:last-child {
            margin-bottom: 0;
        }

        .accordion-panel p {
            padding: 20px;
            font-size: 14px;
        }

        .arrow {
            position: absolute;
            -webkit-transform: translate(-6px, 0);
            transform: translate(-6px, 0);
            margin-top: 26px;
            right: 0;
            padding-right: 20px;
            cursor: pointer;
        }

        .arrow:before, .arrow:after {
            content: "";
            transition: all 0.25s ease-in-out;
            position: absolute;
            background-color: black;
            width: 3px;
            height: 9px;
        }

        .is-open > .arrow:before, .is-open > .arrow:after {
            background-color: white;
        }

        .is-open > .arrow:before {
            -webkit-transform: translate(-2px, 0) rotate(45deg);
            transform: translate(-2px, 0) rotate(45deg);
        }

        .arrow:before {
            -webkit-transform: translate(2px, 0) rotate(45deg);
            transform: translate(2px, 0) rotate(45deg);
        }

        .is-open > .arrow:after {
            -webkit-transform: translate(2px, 0) rotate(-45deg);
            transform: translate(2px, 0) rotate(-45deg);
        }

        .arrow:after {
            -webkit-transform: translate(-2px, 0) rotate(-45deg);
            transform: translate(-2px, 0) rotate(-45deg);
        }

        img.center {
            display: block;
            margin: 0 auto;
        }
        iframe.center {
            display: block;
            margin: 0 auto;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card" style="background-color: #f0f0f0 !important;">
                    <div class="card-header cop-h-f-weight">
                        FAQ
                    </div>
                    <div class="card-body faq-full-body faq-body-color">



                        <div class="row justify-content-center">
                            <div class="col-md-12">

                                <div class="result">
                                    <div class="accordion js-accordion">

                                        @foreach($faqs as $key => $item)
                                            <div class="accordion-item">
                                                <i class="arrow"></i>
                                                <div class="accordion-toggle">{{$item->question}}</div>
                                                <div class="accordion-panel">

                                                    <a href="{!! route("faq.edit", $item->id) !!}"><i class="fas fa-pencil-alt"
                                                                                                      style="margin: 1em"></i> Edit</a>
                                                    <a onclick="return confirm('Are you sure you want to delete?');"
                                                       href="{!! route('faq.delete', @$item->id) !!}" style="margin: 1em; color: red">
                                                        <i class="fa fa-trash-o "> Delete</i>
                                                    </a>


                                                    <p>{{$item->answer}}</p>

                                                    <div class="row">

                                                        @if($item->image != '')
                                                            @if($item->youtube_link == '')
                                                                <div class="col-md-12 col-12 col-sm-12">
                                                                    @else
                                                                        <div class="col-md-6 col-12 col-sm-12">
                                                                            @endif
                                                                            <br>
                                                                            <img src="{{asset('uploads/faq').'/'.$item->image}}" class="w-75 center">
                                                                            <br>
                                                                        </div>
                                                                    @endif

                                                                    @if($item->youtube_link != '')

                                                                        @if($item->image == '')
                                                                            <div class="col-md-12 col-12 col-sm-12">
                                                                                @else
                                                                                    <div class="col-md-6 col-12 col-sm-12">
                                                                                        @endif

                                                                                        <br>
                                                                                        <iframe id="iframe_link" {{--height="300" width="300"--}} frameborder="0"
                                                                                                style="margin-top: 5px; height: 250px" allowfullscreen
                                                                                                src="https://www.youtube.com/embed/{{$item->youtube_link}}"
                                                                                                class="w-75 center">
                                                                                        </iframe>
                                                                                        <br>
                                                                                        @endif
                                                                                    </div>



                                                                            </div>
                                                                </div>
                                                                @endforeach

                                                    </div>
                                                </div>
                                            </div>
                            </div>

                            {!! $faqs->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-JS')

    <script>
        // On DOM ready,
        $(document).ready(function () {
            // Cache selectors
            var $accordion = $(".js-accordion");
            var $allPanels = $(" .accordion-panel").hide();
            var $allItems = $(".accordion-item");

            // Event listeners
            $accordion.on("click", ".accordion-toggle", function () {
                // Toggle the current accordion panel and close others
                $allPanels.slideUp();
                $allItems.removeClass("is-open");
                if (
                    $(this).next().is(":visible")) {
                    $(".accordion-panel").slideUp();
                } else {
                    $(this).next().slideDown().closest(".accordion-item").addClass("is-open");
                }
                return false;
            });
        });
    </script>
@endsection