@extends('layouts.app')

@section('content')
<script src="https://cdn.ckeditor.com/ckeditor5/12.0.0/classic/ckeditor.js"></script>
<style type="text/css">
    .time-and-true{
        border: 1px solid #eee;
        text-align: center;
        margin-top: 8px;
        padding:3px;
    }
    .middle-div{
            background: #eee;
            margin: 41px 1px 20px -6px;
            height: 119px;
            padding: 13px;
    }
    .time-left-div{
        background: #f9f7f7;
        padding: 5px;
        height: 45px;
    }
    .referral-div{

    }
    .referral-div input{
    width: 20%;
    padding: 0px 3px 3px 5px;
    margin: 0px 0px 0px 106px; 
    }
    .referral-div h6{
        font-weight: 700;
        font-size: .99rem;
        display: inline;
    }
     h6{
        font-size: .80rem;
    }
    h5{
        font-size: .95rem;
    }
    .user-image {
		border-radius:50%;
	}
    .product-image{
        height: 190px;
        width:330px;
        border-radius: 5%;
        box-shadow: 1px 1px 1px 1px #7b4aef;
	}
    .want-to-buy{
    width: 50% !important;
     padding: 0px;
     margin-top: 11px;
     background: #ffffff;

    }
    .product-name{
         width: 100%;
    font-weight: 800;
   
    text-align: center;
    }
    .shirt{
    width: 25% !important; 
    padding: 0px;
    margin-top: 11px;
    background: #ffffff;
    color: #9e9e9e
    }
    .deliver-time-input{
        width: 10%;
        color: #9e9e9e
    }
      .deliver-time-div{
       display: inline;
       margin-left: 60px;
    }
    .hour{
        width: 25% !important; 
        padding: 0px;
        margin-top: 11px;
        background: #ffffff;
        color: #9e9e9e
    }
    .product-image-ins{
	    width: 328px !important;
    	height: 186px !important;
    	border-radius: 5%;
    }
@media screen and (max-width: 480px) {
	.user-image {
		border-radius:50%;
	}
	.product-image{
        height: 165px;
        width: 213px;
        border-radius: 5%;
      
        box-shadow: 1px 1px 1px 1px #7b4aef;
	}
	 .product-image-ins{
	  	 width: 213px !important;
    	height: 165px !important;
    	border-radius: 5%;
    }
    .product-name{
    width: 100%;
    font-weight: 800;
   
    text-align: center;
    }
    .want-to-buy{
    width: 98% !important;
     padding: 0px;
     margin-top: 11px;
     background: #ffffff;

    }
    .middle-div{
           background: #eee;
    margin: 27px -24px 24px -8px;
    height: 119px;
    padding: 13px;
    }
     .referral-div input{
   width: 28%;
    padding: 1px 10px 2px 14px;
    margin: 5px 3px 42px 4px;
    }
    .shirt{
    width: 65% !important; 
    padding: 0px;
    margin-top: 11px;
    background: #ffffff;
    color: #9e9e9e
    }
     .deliver-time-input{
        width: 30%;
        color: #9e9e9e
    }
     .deliver-time-div{
       display: inline;margin-left: 2px;
    }
     .hour{
        width: 75% !important; 
        padding: 0px;
        margin-top: 1px;
        background: #ffffff;
        color: #9e9e9e
    }
}
</style>
{{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}

<div class="container">
    <div class="card">
        <div class="card-header">
			@php
				$a=Auth::user()->id;
				$a_u=App\User::find($a);

			@endphp
           <img src="{{'/uploads/avatars/' .$a_u->avatar}}" class="user-image float-left"  height="50px" width="50px">
           <h3 style="display: inline">{{$a_u->name}}</h3>
           <a href="{{url('/home')}}" style="float:right"><i class="fas fa-times"></i></a>

        </div>
        <div class="card-body">
                <form action="{{url('save-bid-form')}}" method="post" id="target" enctype="multipart/form-data">
                	@csrf
                	<div class="col-md-4 float-left">
                		{{-- <img src="{{asset('img/profile.jpg')}}" height="190px" width="330px" style="border-radius: 5%; box-shadow: 1px 1px 1px 1px #eee"> --}}
                		<div class="product-image">
                			<img id="blah" src="#" alt="your image" class="product-image-ins" height="100px" width="100px" />
                		</div>
                		
                		<input type="file" name="image" class="" style="" onchange="readURL(this);">
                		
                		<div class="dropdown">
							  <button class="dropdown-toggle want-to-buy" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="">
							   Want to buy
							  </button>
							  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
							    <button class="dropdown-item" type="button">Action</button>
							    <button class="dropdown-item" type="button">Another action</button>
							    <button class="dropdown-item" type="button">Something else here</button>
							  </div>
						</div>
						<div class="time-and-true ">
							<input type="text" name='product_name' class="product-name" required>
						</div>
                		
                		<div class="middle-div">
                			<h6 style="display: inline">Current bid </h6> <h4 style="display: inline">US $</h4>
                			 <input type="number" name="current_bid" style="width: 15%;display: inline" required><br>
                			 <input type="checkbox" value="1" name="auto_order" required><span>Auto-Order</span>
                			 <br>
                			 <div class="time-left-div">
                			 	 <h5 style="display: inline"> Time left</h5>
                			 	 <div style="display: inline;border: 1px solid #d8d6d6;padding: 6px;float: right;"> 2houres 2 min</div>
                			 </div>

                		</div>
                		<div class="referral-div">
                			<div style="display: inline"><input type="text" name="referral" required=""></div>
                			<h6 style="display: inline">% Referral</h6>
                			<i class="fas fa-question-circle"></i>
                		</div>
                	</div>
                	<div class="col-md-8 float-left">
                		<div style="color: #c1c1c1">
	                		<h6 style="text-decoration: underline;">Delivery address </h6>
	                		<input type="text" style="width:55%;color: #9e9e9e;" name="receiver" required>
	                		<div class="form-group" style="margin: 8px 3px 4px 5px;">
	                			<label>Address :</label>
	                			<input type="text" name="delivery_address" style="width: 70%;color: #9e9e9e;" required>
	                		</div>
	                		<div class="form-group">
	                			<label>City</label>
	                			<input type="text" value="Vania" name="city" style="display: inline" required>
	                			<div class="dropdown" style="display: inline">
									  <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 45% !important; padding: 0px;margin-top: 11px;background: #ffffff;color: #9e9e9e;">
									  USA
									  </button>
									  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
									    <button class="dropdown-item" type="button">Action</button>
									    <button class="dropdown-item" type="button">Another action</button>
									    <button class="dropdown-item" type="button">Something else here</button>
									  </div>
								</div>
	                		 </div>
	                		 <div>
	                		 	<label>Phone Number:</label>
	                		 	<input type="text" value="000000000" name="phone_number" style="color: #9e9e9e;" required>
	                		 </div>
	                		<div>
	                		 	<div class="dropdown" style="display: inline">
									  <button class="dropdown-toggle shirt" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="">
									 Shirt
									  </button>
									  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
									    <button class="dropdown-item" type="button">Action</button>
									    <button class="dropdown-item" type="button">Another action</button>
									    <button class="dropdown-item" type="button">Something else here</button>
									  </div>
								</div>
								<div class="form-group deliver-time-div" style="">
									<label>Delivery Time</label>
									<input type="text" class="deliver-time-input" name="delivery_time" required>
								</div>
								<div class="dropdown hour" style="display: inline">
									  <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="">
									 Hour
									  </button>
									  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
									    <button class="dropdown-item" type="button">Action</button>
									    <button class="dropdown-item" type="button">Another action</button>
									    <button class="dropdown-item" type="button">Something else here</button>
									  </div>
								</div>

	                		</div>

                		</div>
                		<textarea name="content" id="editor" required></textarea>
                	</div>
                	

                </form>
        </div>
        <div class="card-footer">
	     <a class="btn" onClick="sub()" style="display: inline;float: right;" id="other"> Save</a>
	     <a class="btn" href="{{url('/bids/buyers')}}" style="display: inline;float: right;"> Cancel</a>
	  	</div>
             
    </div>
</div>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
<script type="text/javascript">
    function sub(){
    	$( "#target" ).submit();
    }
  

</script>
<script type="text/javascript">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection