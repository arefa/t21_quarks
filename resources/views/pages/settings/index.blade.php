@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            Email Sender
        </div>
        <div class="card-body">
            <form method="POST" action="{{ url('settings/env') }}">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="mail_username" value="{{ $setting->mail_username ?? '' }}">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="text" class="form-control" id="password" placeholder="Password" name="mail_password" value="{{ $setting->mail_password ?? '' }}">
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
    
    <br>
    <br>

    <div class="card">
        <div class="card-header">
            Pusher Credentials
        </div>
        <div class="card-body">
            <form method="POST" action="{{ url('settings/env') }}">
                <div class="form-group">
                    <label for="PUSHER_APP_ID">APP_ID</label>
                    <input type="text" class="form-control" id="PUSHER_APP_ID" placeholder="Enter APP_ID" name="pusher_app_id" value="{{ $setting->pusher_app_id ?? '' }}">
                </div>
                <div class="form-group">
                    <label for="PUSHER_APP_KEY">APP_KEY</label>
                    <input type="text" class="form-control" id="PUSHER_APP_KEY" placeholder="Enter APP_KEY" name="pusher_app_key" value="{{ $setting->pusher_app_key ?? '' }}">
                </div>
                <div class="form-group">
                    <label for="PUSHER_APP_SECRET">APP_SECRET</label>
                    <input type="text" class="form-control" id="PUSHER_APP_SECRET" placeholder="Enter APP_SECRET" name="pusher_app_secret" value="{{ $setting->pusher_app_secret ?? '' }}">
                </div>
                <div class="form-group">
                    <label for="PUSHER_APP_CLUSTER">APP_CLUSTER</label>
                    <input type="text" class="form-control" id="PUSHER_APP_CLUSTER" placeholder="Enter APP_CLUSTER" name="pusher_app_cluster" value="{{ $setting->pusher_app_cluster ?? '' }}">
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>

    <br><br>
    <div class="card">
        <div class="card-header">
            Upload file size
        </div>
        <div class="card-body">
            <form method="POST" action="{{ url('settings/env') }}">
                <div class="form-group">
                    <!-- <label for="file_size">Email</label> -->
                    <input type="text" class="form-control" id="file_size" placeholder="Enter file size" name="file_size" value="{{ $setting->file_size ?? '' }}">
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>

    <br><br>
    <div class="card">
        <div class="card-header">
            About us
        </div>
        <div class="card-body">
            <form method="POST" action="{{ url('settings/env') }}">
                <div class="form-group">
                    <!-- <label for="about">Email</label> -->
                    <!-- <input type="text" class="form-control" id="about" placeholder="Enter about..." name="about" value="{{ $setting->about ?? '' }}"> -->
                    <textarea class="form-control form-control-lg " id="editor" name="about" rows="8">{{ $setting->about ?? '' }}</textarea>
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script src="https://pixlcore.com/demos/webcamjs/webcam.min.js"></script>

@endsection