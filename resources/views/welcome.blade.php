@extends('layouts.app')

@section('content')
    <div class="container">
        <style type="text/css">
            /*.hoverSet :hover {
                 border: 5px solid ;
                 overflow: hidden;
             }*/
            .hoverSet:hover .set {

                border: 4px solid orange;
                padding: 2px;

            }

            #savedButton {
                display: none;
            }

            #eventDelete {
                display: none;
            }

            #eventEdit {
                display: none;
            }

            .hoverSet:hover #savedButton {
                display: block;
            }

            .hoverSet:hover #eventDelete {
                display: block;
            }

            .hoverSet:hover #eventEdit {
                display: block;
            }

            .overlay_badge_buy:hover .editButton {
                border: 1px solid orange;
                padding: 2px;
            }

            .overlay_badge_buy:hover .deleteButton {
                border: 1px solid orange;
                padding: 2px;
            }

            .overlay_badge_buy:hover .savedButton {
                border: 1px solid orange;
                padding: 2px;
            }
        </style>
        <style type="text/css">
            .overlay_badge_sell {
                position: absolute;
                top: 10px;
                left: 10px;
                background-color: skyblue;
                color: white;
                padding-left: 10px;
                padding-right: 10px;
            }

            .overlay_badge_buy {
                position: absolute;
                float: right;
                top: 10px;
                left: 10px;
                background-color: green;
                color: white;
                pointer-events: auto;
                padding-left: 10px;
                padding-right: 10px;
            }

            .divback {
                background-color: #e3e3e3;
                padding: 5px;
                margin: 5px;
            }

            .bigtext {
                color: black;
                font-weight: bolder;
            }

            .ordertext {
                color: #a658a6;
            }

            .bidtext {
                color: #4e66c4;
                align-self: flex-end;
            }
        </style>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="row">
                    @isset($buyers)
                        @foreach($buyers as $buyer)
                            <?php
                            $checkPermission = DB::table('user_menu')->where('menu_options_id', '=', '18')->where('user_id', '=', auth()->id())->get()->first();
                            if ($checkPermission) {
                                $permission = 1;
                            } else {
                                $permission = 0;
                            }
                            ?>
                            @php $bidAmount=bidAmountIfAlreadyBid($buyer,'buy');
                           $isPostSaved=isSavedPost($buyer->id,'buy',auth()->id());
                            @endphp
                            <div class="col-md-3 hoverSet">
                                <div class="card mb-3 set" style="width: 17rem;">
                                    <a href="#" data-bids="{{getPostTotalBids($buyer,'buy')}}"
                                       data-orders="{{getPostBidOrders($buyer,'buy')}}" data-isSaved="{{$isPostSaved}}"
                                       data-user-name="{{$buyer->user->name}}" data-avatar="{{$buyer->user->avatar}}"
                                       data-all="{{json_encode($buyer)}}"
                                       onclick="showPostDetails('{{$buyer->id}}','{{auth()->id()}}',this)">
                                        <img class="card-img-top"
                                             src="{{$buyer->buyer_featured_image?"/uploads/buyer/".$buyer->buyer_featured_image:"/images/image_not_found.jpg"}}"
                                             style="padding: 10px;" alt="Card image cap">
                                    </a>
                                    <span class="overlay_badge_buy">Buy</span>
                                    @if(auth()->id()==$buyer->user_id || $permission==1)
                                        <span class="overlay_badge_buy" id="buyDelete"
                                              style="background: none;padding: 0;margin-left: 145px;margin-top: 2px">
                                         <a href="#" onclick="deleteBuy('{{$buyer->id}}')">
                                              <img src="/images/close.png" style="height: 30px;"
                                                   class="deleteButton img-thumbnail">
                                          </a>
                                </span>
                                        <span class="overlay_badge_buy" id="buyEdit"
                                              style="background: none;margin-left: 180px;padding: 0;margin-top: 2px">
                                                            <a href="#"
                                                               onclick="buyEdit('{{$buyer->id}}','{{$buyer->user_id}}')"
                                                               class=""><img src="/images/edit.png"
                                                                             style="height: 30px;"
                                                                             class=" img-thumbnail"></a>
                                                        </span>
                                    @endif
                                    <span class="overlay_badge_buy" id="savedButton"
                                          style="background: none;padding: 0;margin-left: 215px;margin-top: 2px">
                                 <a href="#" onclick="saveBuySell('{{$buyer->id}}','{{auth()->id()}}','buy')">
                                 <img src="{{$isPostSaved?'/images/rating.png':'/images/rating_blank.png'}}"
                                      style="height: 30px;" id="savedBuy{{$buyer->id}}"
                                      class="savedButton img-thumbnail">
                                 </a>
                            </span>


                                    <strong style="align-self: center;">{{$buyer->buyer_pro_title}}</strong>
                                    <div class="divback">
                                        <label style="padding-right: 16px;">Current rate : <big class="bigtext"> US
                                                ${{getCurrentRate($buyer)}}</big></label><label
                                                class="ordertext float-right">
                                            @if($buyer->user_id==auth()->id())
                                                <a onclick="buySellOrder('{{$buyer->id}}','{{getPostBidOrders($buyer,'buy')}}')"
                                                   href="#">[ {{getPostBidOrders($buyer)}} orders] </a>
                                            @else
                                                [ {{getPostBidOrders($buyer)}} orders]
                                            @endif
                                        </label><br>
                                        <div style="flex-flow: column;">

                                            <label style="align-self: flex-start;">{{--Auto order--}}</label>
                                            <input type="text" value="{{$bidAmount}}" class="bidinput"
                                                   data-id="{{$buyer->id}}" size="4"
                                                   style="align-self: center;">
                                            <button data-id="{{$buyer->id}}" data-max="{{getCurrentRate($buyer)}}"
                                                    class="triggerBid"
                                                    style="background-color: #0055a2;border-color: #0055a2;color: #fff">
                                                Bid
                                            </button>
                                            <button data-id="{{$buyer->id}}" data-post-type='buy'
                                                    class="closebidinput {{$bidAmount?'typing':''}}"
                                                    style="color: #fff;margin-right: 35px;"><i
                                                        style="display: none" class="fas fa-close"></i></button>
                                            <label class="bidtext float-right">
                                                @if($buyer->user_id==auth()->id())
                                                    <a onclick="buySellBids('{{$buyer->id}}','{{getPostTotalBids($buyer,'buy')}}')"
                                                       href="#">[ {{getPostTotalBids($buyer)}} bid ]</a>
                                                @else
                                                    [ {{getPostTotalBids($buyer)}} orders]
                                                @endif
                                            </label>
                                            <h6 data-time="{{$buyer->created_at->addHours($buyer->hour)}}"
                                                class="countDownTimer" id="showCountDownTimer"
                                                style="text-align: center; background-color: #f8f8f8">{{$buyer->hour}}</h6>
                                            <div style="padding: 5px;border-radius: 2px;">
                                                <i class="fas fa-share" style="font-size: 25px;color: #00a3e9"></i>&nbsp;345&nbsp;&nbsp;<span
                                                        class="float-right" style="font-size: 20px;">{{$buyer->buyer_commission_percentage}}% Refferal</span>
                                            </div>
                                            <div style="text-align: center">
                                                <span>{{$buyer->buyer_location}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endisset
                    @isset($events)
                        @foreach($events as $event)
                            <?php
                            // $userId = Auth::user()->id;
                            $paydate_raw = DB::raw("STR_TO_DATE(`event_date`, '%m/%d/%Y')");
                            $currDate = date('m/d/Y');
                            $getEventDateSavePost = App\EventModal::where('event_date', ">=", $currDate)->where('event_id', $event->id)->orderBy('event_date', 'asc')->get()->first();


                            ?>

                            <?php
                            if($getEventDateSavePost)
                            {
                            ?>
                            <div class="col-md-3 hoverSet">
                                <div class="card mb-3 set" style="width: 17rem;">
                                    <?php
                                    if(empty($event->event_modal_image))
                                    {
                                    ?>
                                    <a href="/home" id=""><img class="card-img-top" src="/images/image_not_found.jpg"
                                                               style="padding: 10px;height: 200px" alt="Card image cap"></a>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    <a href="/home" id=""><img class="card-img-top"
                                                               src="/uploads/event/{{ $event->event_modal_image }}"
                                                               style="padding: 10px;height: 200px" alt="Card image cap"></a>
                                    <?php
                                    }
                                    ?>


                                    <span class="overlay_badge_buy" id="" style="background: none;padding: 0;">
                                     <img src="/images/eventLogo.png" id="" style="height: 40px;">
                                    </span>

                                    <span class="overlay_badge_buy" id="savedButton"
                                          style="background: none;padding: 0;margin-left: 215px;margin-top: 2px">
                                    
                                    
                                         <a href="/home">
                                         <img src="/images/rating_blank.png" style="height: 30px;"
                                              id="savedImage<?php echo $event->id; ?>"
                                              class="savedButton img-thumbnail">
                                         </a>
                                    
                                   
                                  
                                    </span>
                                    <strong style="align-self: center;"><a
                                                href="/home">{{ $event->event_title }}</a></strong>
                                    <div class="divback">
                                        <label style="padding-right: 33px;background: lightgray"> <big class="bigtext">
                                                <?php
                                                if ($event->event_fee_type == 'Not Free') {
                                                    echo '$ ' . $event->event_fee;
                                                } else {
                                                    echo $event->event_fee_type;
                                                }

                                                ?>

                                            </big></label>
                                        <label class="ordertext float-right">{{ $event->event_city }}
                                            , {{ $event->event_country }}</label><br>
                                        <div style="flex-flow: column;">
                                            <label style="align-self: flex-start;background: #ffffff;padding: 5px"><span
                                                        style="font-size: 20px;"><span style="color: red">
                                            {{ date('M j, Y', strtotime($getEventDateSavePost->event_date)) }}
                                            </span><br/> </span></label>
                                            <label style="align-self: flex-start;float: right">


                                                <img = src="/images/notgoing.png"  style="width: 80px
                                                ;
                                                    height: 30px"/>

                                            </label>
                                            <div style="padding: 5px;border-radius: 2px;background: white"><a href="#">
                                                    <i class="fas fa-share" style="font-size: 25px;color: #00a3e9"></i></a>&nbsp;345&nbsp;&nbsp;<span
                                                        class="float-right" style="font-size: 20px;">{{ $event->event_referral_commission }}% Refferal</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php  } ?>
                        @endforeach
                    @endisset
                </div>
            </div>
        </div>
    </div>
@endsection
